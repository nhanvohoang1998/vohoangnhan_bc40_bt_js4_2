// Bài 5: Tính ngày tháng năm 
/**
 * input: nhập vào ngày, tháng, năm
 * handle: viết function để kiểm tra năm nhuận
 * dùng switch case để chia nhỏ các trường hợp
 * output: Xuất ra ngày hôm qua hoặc ngày mai tùy theo lựa chọn của người nhập
 */
function kiemTraNamNhuan(nhapNam) {
    if ((nhapNam % 4 == 0 && nhapNam % 100 != 0) || nhapNam % 100 == 0) {
        return true;
    }
    return false;
};

function kiemTraNgayThangNam(nhapNgay, nhapThang, nhapNam) {
    if (kiemTraNamNhuan(nhapNam)) {
        if (nhapThang > 0 && nhapThang <= 12) {
            if (nhapThang == 2) {
                if (nhapNgay > 0 && nhapNgay <= 29) {
                    return true;
                } else {
                    return false;
                }
            } else if (nhapThang == 4 || nhapThang == 6 || nhapThang == 9 || nhapThang == 11) {
                if (nhapNgay > 0 && nhapNgay <= 30) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (nhapNgay > 0 && nhapNgay <= 31) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    } else {
        if (nhapThang > 0 && nhapThang <= 12) {
            if (nhapThang == 2) {
                if (nhapNgay > 0 && nhapNgay <= 28) {
                    return true;
                } else {
                    return false;
                }
            } else if (nhapThang == 4 || nhapThang == 6 || nhapThang == 9 || nhapThang == 11) {
                if (nhapNgay > 0 && nhapNgay <= 30) {
                    return true;
                } else {
                    return false;
                }
            } else {
                if (nhapNgay > 0 && nhapNgay <= 31) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
};

var ngayHomQua = document.getElementById("ngayHomQua");
ngayHomQua.addEventListener("click", function () {
    var nhapNgay = document.getElementById('nhapNgay').value * 1;
    var nhapThang = document.getElementById('nhapThang').value * 1;
    var nhapNam = document.getElementById('nhapNam').value * 1;
    var ketQua = document.getElementById("ketQua");
    if (kiemTraNgayThangNam(nhapNgay, nhapThang, nhapNam)) {
        if (nhapNgay == 1) {
            switch (nhapThang) {
                case 5:
                case 7:
                case 10:
                case 12: {
                    ketQua.innerHTML = `Kết quả: ${30}/${nhapThang - 1}/${nhapNam}`
                    break;
                }

                case 1: {
                    ketQua.innerHTML = `Kết quả: ${31}/${12}/${nhapNam - 1}`
                    break;
                }
                case 2:
                case 4:
                case 6:
                case 8:
                case 9:
                case 11: {
                    ketQua.innerHTML = `Kết quả: ${31}/${nhapThang - 1}/${nhapNam}`
                    break;
                }
                case 3: {
                    if (kiemTraNamNhuan(nhapNam)) {
                        ketQua.innerHTML = `Kết quả: ${29}/${nhapThang - 1}/${nhapNam}`
                    } else {
                        ketQua.innerHTML = `Kết quả: ${28}/${nhapThang - 1}/${nhapNam}`
                    }
                    break;
                }
            }
        } else {
            ketQua.innerHTML = `Kết quả: ${nhapNgay - 1}/${nhapThang}/${nhapNam}`
        }
    } else {
        alert("Kiểm tra lại ngày tháng năm đã nhập!")
    }
});

var ngayMai = document.getElementById("ngayMai");
ngayMai.addEventListener("click", function () {
    var nhapNgay = document.getElementById('nhapNgay').value * 1;
    var nhapThang = document.getElementById('nhapThang').value * 1;
    var nhapNam = document.getElementById('nhapNam').value * 1;
    var ketQua = document.getElementById("ketQua");

    if (kiemTraNgayThangNam(nhapNgay, nhapThang, nhapNam)) {
        if (nhapNgay == 31) {
            switch (nhapThang) {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10: {
                    ketQua.innerHTML = `Kết quả: ${1}/${nhapThang + 1}/${nhapNam}`
                    break;
                }
                case 12: {
                    ketQua.innerHTML = `Kết quả: ${1}/${1}/${nhapNam + 1}`
                    break;
                }
            }
        } else if (nhapNgay == 30) {
            switch (nhapThang) {
                case 4:
                case 6:
                case 9:
                case 11: {
                    ketQua.innerHTML = `Kết quả: ${1}/${nhapThang + 1}/${nhapNam}`
                    break;
                }
                default: {
                    ketQua.innerHTML = `Kết quả: ${nhapNgay + 1}/${nhapThang}/${nhapNam}`
                }

            }
        } else if (nhapNgay == 28) {
            if (nhapThang == 2) {
                if (kiemTraNamNhuan(nhapNam)) {
                    ketQua.innerHTML = `Kết quả: ${nhapNgay + 1}/${nhapThang}/${nhapNam}`
                } else {
                    ketQua.innerHTML = `Kết quả: ${1}/${3}/${nhapNam}`
                }
            }
        } else if (nhapNgay == 29) {
            if (nhapThang == 2) {
                if (kiemTraNamNhuan(nhapNam)) {
                    ketQua.innerHTML = `Kết quả: ${1}/${3}/${nhapNam}`
                } else {
                    alert("nhập sai ngày xin nhập lại");
                }
            }
        } else {
            ketQua.innerHTML = `Kết quả: ${nhapNgay + 1}/${nhapThang}/${nhapNam}`
        }
    } else {
        alert("Kiểm tra lại ngày tháng năm đã nhập!");
    }
});


// Bài 6: Tính ngày 
/**
 * input: nhập tháng, năm
 * handle: sử dụng switch case để chia làm 12 tháng và hàm kiểm tra năm nhuận để xác định năm đó có phải năm nhuận không
 * output: Xuất ra số ngày có trong tháng
 */

var tinhNgay = document.getElementById('tinhNgay');
tinhNgay.addEventListener("click", function () {
    var nhapThang_bt6 = document.getElementById('nhapThang_bt6').value * 1;
    var nhapNam_bt6 = document.getElementById('nhapNam_bt6').value * 1;
    var ketQua_bt6 = document.getElementById("ketQua_bt6");

    if (nhapThang_bt6 > 0 && nhapThang_bt6 <= 12) {
        switch (nhapThang_bt6) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12: {
                ketQua_bt6.innerHTML = `Kết quả: Tháng ${nhapThang_bt6} năm ${nhapNam_bt6} có ${31} ngày`
                break;
            }
            case 4:
            case 6:
            case 9:
            case 11: {
                ketQua_bt6.innerHTML = `Kết quả: Tháng ${nhapThang_bt6} năm ${nhapNam_bt6} có ${30} ngày`
                break;
            }
            case 2: {
                if (kiemTraNamNhuan(nhapNam_bt6)) {
                    ketQua_bt6.innerHTML = `Kết quả: Tháng ${nhapThang_bt6} năm ${nhapNam_bt6} có ${29} ngày`
                } else {
                    ketQua_bt6.innerHTML = `Kết quả: Tháng ${nhapThang_bt6} năm ${nhapNam_bt6} có ${28} ngày`
                }
                break;
            }
        }
    } else {
        alert("Kiểm tra lại tháng đã nhập!")
    }
});

// Bài 7: Đọc số 
/**
 * input: nhập vào số có 3 chữ số
 * handle: Viêt hàm đọc số hàng trăm, chục, đơn vị và xử lý trường hợp nếu hàng đơn vị và hàng chục đều bằng 0
 * output: đọc số có 3 chữ số
 */
function docSoHangTram(soHangTram) {
    switch (soHangTram) {
        case 1: {
            return "Một trăm";
        }
        case 2: {
            return "Hai trăm";
        }
        case 3: {
            return "Ba trăm";
        }
        case 4: {
            return "Bốn trăm";
        }
        case 5: {
            return "Năm trăm";
        }
        case 6: {
            return "Sáu trăm";
        }
        case 7: {
            return "Bảy trăm";
        }
        case 8: {
            return "Tám trăm";
        }
        case 9: {
            return "Chín trăm";
        }
    }
};

function docSoHangChuc(soHangChuc) {
    switch (soHangChuc) {
        case 0: {
            return "lẻ"
        }
        case 1: {
            return "mười";
        }
        case 2: {
            return "hai mươi";
        }
        case 3: {
            return "ba mươi";
        }
        case 4: {
            return "bốn mươi";
        }
        case 5: {
            return "năm mươi";
        }
        case 6: {
            return "sáu mươi";
        }
        case 7: {
            return "bảy mươi";
        }
        case 8: {
            return "tám mươi";
        }
        case 9: {
            return "chín mươi";
        }
    }
};

function docSoHangDonVi(soHangDonVi) {
    switch (soHangDonVi) {
        case 0: {
            return "";
        }
        case 1: {
            return "một";
        }
        case 2: {
            return "hai";
        }
        case 3: {
            return "ba";
        }
        case 4: {
            return "bốn";
        }
        case 5: {
            return "năm";
        }
        case 6: {
            return "sáu";
        }
        case 7: {
            return "bảy";
        }
        case 8: {
            return "tám";
        }
        case 9: {
            return "chín";
        }
    }
};

var docSo = document.getElementById('docSo');
docSo.addEventListener("click", function () {
    var nhapSo3ChuSo = document.getElementById('nhapSo3ChuSo').value * 1;
    var ketQua_bt7 = document.getElementById('ketQua_bt7');
    var soHangTram = Math.floor(nhapSo3ChuSo / 100);
    var soHangChuc = Math.floor(nhapSo3ChuSo % 100 / 10);
    var soHangDonVi = nhapSo3ChuSo % 10;

    if (nhapSo3ChuSo < 1000 && nhapSo3ChuSo > 99) {
        if (soHangChuc == 0 & soHangDonVi == 0) {
            ketQua_bt7.innerHTML = docSoHangTram(soHangTram);
        } else {
            ketQua_bt7.innerHTML = docSoHangTram(soHangTram) + " " + docSoHangChuc(soHangChuc) + " " + docSoHangDonVi(soHangDonVi);
        }
    } else {
        alert("Nhập lại số có 3 chữ số!")
    }
});

// Bài 8: Tìm sinh viên xa trường nhất 
/***
 * inout: nhập vào tên, tọa độ x,y của từng sinh viên và trường học
 * handle: sử dụng hàm tính độ dài quảng đường, sau đó so sánh gán biến tạm để tìm ra sinh vien có quãng đường xa nhất
 * output: xuất ra sinh viên có nhà xa trường nhất
 */

function tinhDoDaiDoanDuong(toaDoX_SinhVien, toaDoY_SinhVien, toaDoX_TruongHoc, toaDoY_TruongHoc) {
    return Math.sqrt((toaDoX_TruongHoc - toaDoX_SinhVien) * (toaDoX_TruongHoc - toaDoX_SinhVien) + (toaDoY_TruongHoc - toaDoY_SinhVien) * (toaDoY_TruongHoc - toaDoY_SinhVien));
}

var timQuangDuongXaNhat = document.getElementById('timQuangDuongXaNhat');
timQuangDuongXaNhat.addEventListener("click", function () {
    var tenSinhVien1 = document.getElementById('tenSinhVien1').value;
    var tenSinhVien2 = document.getElementById('tenSinhVien2').value;
    var tenSinhVien3 = document.getElementById('tenSinhVien3').value;
    var toaDoX_SinhVien1 = document.getElementById('toaDoX_SinhVien1').value * 1;
    var toaDoX_SinhVien2 = document.getElementById('toaDoX_SinhVien2').value * 1;
    var toaDoX_SinhVien3 = document.getElementById('toaDoX_SinhVien3').value * 1;
    var toaDoY_SinhVien1 = document.getElementById('toaDoY_SinhVien1').value * 1;
    var toaDoY_SinhVien2 = document.getElementById('toaDoY_SinhVien2').value * 1;
    var toaDoY_SinhVien3 = document.getElementById('toaDoY_SinhVien3').value * 1;
    var toaDoX_TruongHoc = document.getElementById('toaDoX_TruongHoc').value * 1;
    var toaDoY_TruongHoc = document.getElementById('toaDoY_TruongHoc').value * 1;

    var doanDuongSinhVien1 = tinhDoDaiDoanDuong(toaDoX_SinhVien1, toaDoY_SinhVien1, toaDoX_TruongHoc, toaDoY_TruongHoc)
    var doanDuongSinhVien2 = tinhDoDaiDoanDuong(toaDoX_SinhVien2, toaDoY_SinhVien2, toaDoX_TruongHoc, toaDoY_TruongHoc)
    var doanDuongSinhVien3 = tinhDoDaiDoanDuong(toaDoX_SinhVien3, toaDoY_SinhVien3, toaDoX_TruongHoc, toaDoY_TruongHoc)

    var tmp = tenSinhVien1;
    var max = doanDuongSinhVien1;
    if (max < doanDuongSinhVien2) {
        tmp = tenSinhVien2;
        max = doanDuongSinhVien2;
    }
    if (max < doanDuongSinhVien3) {
        tmp = tenSinhVien3;
        max = doanDuongSinhVien3;
    }

    var ketQua_bt8 = document.getElementById("ketQua_bt8");
    ketQua_bt8.innerHTML = `Sinh viên xa trường nhất: ${tmp}`;
});